# Logic Inputter 

Just a small extension that help students of the JKU to enter logical symboles faster. 

## How to install

### Chromium Based Browsers

As it's not available in an store, you have to manually add it.
Just download the repo and visit [chrome://extensions](chrome://extensions) in chrome, enable dev mode and load unpacked.

### Firefox

As it's not available in the store, you have to manually add it.
Just download the repo and visit [about:debugging#/runtime/this-firefox](about:debugging#/runtime/this-firefox) and load temporary add-on.

## How to use

As soon as you input any text in an input field at [al.risk.jku.at](al.risk.jku.at) the keywords will be replaced within 3 seconds after the last keyboard input.

## Keywordtable

| Keyword | Symbol | 
| --- | --- |
| forall | ∀ |
| exists | ∃ |
| and | ∧ |
|  or |  ∨ |
| not | ¬ |
| st | < |
| lt | < |
| se | ≤ |
| le | ≤ |
| gt | > |
| ge | ≥ |
| eq | = |
| implies | ⇒ |
| iff | ⇔ |
| top | ⊤ |
| bottom | ⊥ |
