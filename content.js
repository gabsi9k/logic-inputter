
var timeout

function registerTextReplacer() {
  const input = document.getElementsByClassName('input');
  for (let i = 0; i < input.length; i++) {
    const elem = input[i];
    elem.addEventListener('input', function (e) {
      if (timeout != null) {
        clearTimeout(timeout)
      }
      timeout = setTimeout(replaceText, 2000)
    });
  }

}

function replaceText() {
  const input = document.getElementsByClassName('input');

  // for (let i = 0; i < input.length; i++) {
  //   const elem = input[i];
  //   elem.addEventListener('input', function (e) {
  //     let orig = e.target.value
  //     orig = orig.replaceAll('forall', '∀')
  //     orig = orig.replaceAll('exists', '∃')
  //     orig = orig.replaceAll('and', '∧')
  //     orig = orig.replaceAll(' or', ' ∨')
  //     orig = orig.replaceAll('not', '¬')
  //     orig = orig.replaceAll('sm', '<')
  //     orig = orig.replaceAll('se', '≤')
  //     orig = orig.replaceAll('bt', '>')
  //     orig = orig.replaceAll('be', '≥')
  //     orig = orig.replaceAll('eq', '=')
  //     orig = orig.replaceAll('implies', '⇒')
  //     orig = orig.replaceAll('iff', '⇔')
  //     orig = orig.replaceAll('top', '⊤')
  //     orig = orig.replaceAll('bottom', '⊥')
  //     e.target.value = orig;
  //   });
  // }
  for (let i = 0; i < input.length; i++) {
    let orig = input[i].value;
    orig = orig.replaceAll('forall', '∀')
    orig = orig.replaceAll('exists', '∃')
    orig = orig.replaceAll('and', '∧')
    orig = orig.replaceAll(' or', ' ∨')
    orig = orig.replaceAll('not', '¬')
    orig = orig.replaceAll('st', '<')
    orig = orig.replaceAll('lt', '<')
    orig = orig.replaceAll('se', '≤')
    orig = orig.replaceAll('le', '≤')
    orig = orig.replaceAll('gt', '>')
    orig = orig.replaceAll('ge', '≥')
    orig = orig.replaceAll('eq', '=')
    orig = orig.replaceAll('implies', '⇒')
    orig = orig.replaceAll('iff', '⇔')
    orig = orig.replaceAll('top', '⊤')
    orig = orig.replaceAll('bottom', '⊥')
    input[i].value = orig;
  }
}
registerTextReplacer()

function setCaretPosition(elemId, caretPos) {
  var elem = document.getElementById(elemId);

  if (elem != null) {
    if (elem.createTextRange) {
      var range = elem.createTextRange();
      range.move('character', caretPos);
      range.select();
    }
    else {
      if (elem.selectionStart) {
        elem.focus();
        elem.setSelectionRange(caretPos, caretPos);
      }
      else
        elem.focus();
    }
  }
}
